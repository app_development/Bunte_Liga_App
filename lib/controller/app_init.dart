import 'package:bunte_liga_app/controller/shared_preferences_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<bool> appInit(bool eraseEverythingFromSP) async {
  // final String databasesPath = await getDatabasesPath();
  // const String importDBFileName = 'myApp.db';

  SharedPreferencesController.setPrefs(await SharedPreferences.getInstance());

  if (eraseEverythingFromSP) {
    SharedPreferencesController.getPrefs().clear();
  }

  if (!SharedPreferencesController.getPrefs().containsKey('isNewLaunch')) {
    firstLaunchInit();
    SharedPreferencesController.getPrefs().setBool('isNewLaunch', false);
  }

  // DBController.setDB(await openDatabase(join(databasesPath, importDBFileName)));

  return true;
}

//String systemDatabasesPath, String dbFileName
Future<bool> firstLaunchInit() async {
  // DBController.copyDatabaseFromAssets(systemDatabasesPath, dbFileName);
  SharedPreferencesController.getPrefs().setInt("currTheme", 0);

  return true;
}
