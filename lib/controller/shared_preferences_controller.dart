import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesController {
  static SharedPreferences? _prefs;

  static SharedPreferences getPrefs() => _prefs!;

  static void setPrefs(SharedPreferences sharedPreferences) =>
      _prefs = sharedPreferences;
}
