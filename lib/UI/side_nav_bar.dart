import 'package:bunte_liga_app/UI/screens/settings_screen.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class SideNavBar extends StatelessWidget {
  const SideNavBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        physics: const NeverScrollableScrollPhysics(),
        padding: EdgeInsets.zero,
        children: [
          const UserAccountsDrawerHeader(
            accountName: Text('AirFlow'),
            accountEmail: Text('AschenballsportChampions'),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(child: Icon(Icons.person_2)),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.table_rows),
            title: Text('table'.tr),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.place),
            title: Text('Platzbuchung'.tr),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.sports_soccer),
            title: Text('Ergebnisse'.tr),
            onTap: () {},
          ),
          const Divider(),
          ListTile(
            leading: const Icon(Icons.settings),
            title: Text('settings'.tr),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const SettingsScreen()));
            },
          )
        ],
      ),
    );
  }
}
