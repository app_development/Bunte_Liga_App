// Get.changeThemeMode(ThemeData)
// ThemeData(extension: ) own Variables
// ThemeData().light.copyWith(changes)
//Color.fromARGB(245, 20, 245, 245)
//Color.fromARGB(255, 20, 245, 245);
// hexCode: #14F5F5
// H: 180, S: 92, V:96; RGB: 20, 245, 245

import 'package:bunte_liga_app/controller/shared_preferences_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// this enum has to have the same order as the themes in _themes (list)
enum Themes { standardLightTheme, standardDarkTheme }

ThemeData standardLightTheme = ThemeData.light().copyWith(
  visualDensity: VisualDensity.adaptivePlatformDensity,
);

ThemeData standardDarkTheme = ThemeData.dark().copyWith(
  visualDensity: VisualDensity.adaptivePlatformDensity,
);

class ThemeManager extends GetxController {
  final List<ThemeData> _themes = [standardLightTheme, standardDarkTheme];

  ThemeData getTheme(Themes theme) => _themes[theme.index];
  ThemeData getThemeFromIndex(int index) => _themes[index];

  void setTheme(Themes theme) {
    setThemeFromIndex(theme.index);
  }

  void setThemeFromIndex(int index) {
    SharedPreferencesController.getPrefs().setInt("currTheme", index);
    Get.changeTheme(_themes[index]);
  }
}
