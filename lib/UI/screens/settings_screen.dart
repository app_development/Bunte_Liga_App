import 'package:bunte_liga_app/UI/side_nav_bar.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: const SideNavBar(),
        appBar: AppBar(title: Text('settings'.tr)),
        body: ListView(
          physics: const NeverScrollableScrollPhysics(),
          children: const [LanguageSetting()],
        ));
  }
}

class LanguageSetting extends StatelessWidget {
  const LanguageSetting({super.key});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.all(20),
      leading: Text(
        'language'.tr,
        style: const TextStyle(fontSize: 18),
      ),
      title: Container(
        child: Row(
          children: [
            SizedBox(
              width: 200,
            ),
            Container(
              child: Text('DE'),
            ),
            SizedBox(
              width: 20,
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20), color: Colors.grey),
              child: const Padding(
                padding: EdgeInsets.all(12.0),
                child: Text('EN'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
