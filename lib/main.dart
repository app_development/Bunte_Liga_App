import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'UI/screens/table_screen.dart';

import 'UI/theme/theme_manager.dart';
import 'controller/app_init.dart';
import 'controller/shared_preferences_controller.dart';
import 'l10n/app_translation.dart';
import 'l10n/localization_controller.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  bool eraseEverythingFromSP = true;
  await appInit(eraseEverythingFromSP);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeManager().getThemeFromIndex(
            SharedPreferencesController.getPrefs().getInt("currTheme")!),
        themeMode: ThemeMode.light,
        translations: AppTranslation(),
        locale: LocalizationController.getStartingLocale(),
        fallbackLocale: const Locale('en'),
        home: const TableScreen());
  }
}
