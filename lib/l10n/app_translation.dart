import 'package:get/get.dart';

class AppTranslation extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_EN': {
          "language": "english",
          'settings': "Settings",

          // sideNavBar
          'table': "Table",
          'field_reservation': "Field reservation",
          'results': "Results",
        },
        'de_DE': {
          "language": "Deutsch",
          'settings': "Einstellungen",

          // sideNavBar
          'table': "Tabelle",
          'field_reservation': "Platzbuchung",
          'results': "Ergebnisse",
        }
      };
}
