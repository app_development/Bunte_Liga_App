# Bunte Liga App

## Name
BL App

## Description
Coming soon

## Installation
Go to the App Store/ Play Store and download it.

## Support
Visit our website: https://www.bunteliga.de

## Roadmap
Release 2025

## Authors and acknowledgment
AirFlow

## License
This is not an open-source project. By this in default - all rights are reserved -. Meaning that no one is allowed to make any copy/ sell/ modify/etc. of this Code. 

## Project status
Still in progress. I am on it. I am developing this App on my own to provide a nice on top experience for every player playing in the Bunte Liga.
